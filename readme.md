Projekt akademicki - aplikacja Universal Windows

Aplikacja wykorzystuje wcześniej stworzone API. 

Aplikacja umożliwia:
* logowanie użytkownika,
* wylogowywanie użytkownika,
* rejestrację użytkownika,
* śledzenie przesyłek zarejestrowanych w systemie,
* widok kuriera - możliwość zmiany statusu przesyłki,
* widok pracownika - dodawanie nowych przesyłek + możliwości kuriera
* widok adminisratora - edycja bazy adresów + możliwości pracownika
* widok roota - edycja i dodawanie nowych statusów przesyłek, edycja poziomu użytkownika + możliwości administratora.


Użytkownicy standardowi:
login/password
root/root
JanKow/jankow123
TesTes/testes123
AndKru/andkru123