﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace uniMobileUWP
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class TrackPage : Page
    {
        private ObservableCollection<oldStatus> _oldStatuses = new ObservableCollection<oldStatus>();
        public ObservableCollection<oldStatus> OldStatuses { get { return _oldStatuses; } }

        private ObservableCollection<Models.Status> _allStatuses = new ObservableCollection<Models.Status>();
        private void updateAllStatuses(List<Models.Status> ls)
        {
            _allStatuses.Clear();

            foreach(Models.Status s in ls)
            {
                _allStatuses.Add(s);
            }
        }
        private void udpdateOldStatuses(Models.OldStatuses os)
        {
            _oldStatuses.Clear();

            var dates = os.ListOfDates.Split(',');
            var statuses = os.ListOfIds.Split(',');

            for(int i = 0; i<dates.Length; i++)
            {

                oldStatus old = new oldStatus()
                {
                    timeOfChange = DateTime.ParseExact(dates[i], "yyyyMMddHHmm", CultureInfo.InvariantCulture),
                    name = _allStatuses.FirstOrDefault(e => e.StatusId.Equals(int.Parse(statuses[i]))).Name,
                    description = _allStatuses.FirstOrDefault(e => e.StatusId.Equals(int.Parse(statuses[i]))).Description
                };

                _oldStatuses.Add(old);
            }
        }
        public TrackPage()
        {
            this.InitializeComponent();
            updateAllStatuses(Models.APIConnector.GetPackageStatuses());
        }

        async private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                int trackingNumber = int.Parse(TrackingNumber_TextBox.Text);

                //var package = Models.APIConnector.GetPackage(trackingNumber);

                var oldStatuses = Models.APIConnector.TrackPackage(trackingNumber);

                udpdateOldStatuses(oldStatuses);

                tracking_Panel.Visibility = Visibility.Visible;
            }
            catch (Exception exception)
            {
                await new MessageDialog(exception.ToString(), "Error").ShowAsync();
            }
        }
    }

    public class oldStatus
    {
        public string name;
        public string description;
        public DateTime timeOfChange;
    }
}
