﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using uniMobileUWP.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace uniMobileUWP
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class ModifyAddressPage : Page
    {
        private ObservableCollection<Address> _addresses = new ObservableCollection<Address>();
        private Address _editingAddress;
        private bool _actualizing = false;
        public ObservableCollection<Address> Addresses { get { return _addresses; } }

        private void setAddressess(List<Address> aList)
        {
            _addresses.Clear();

            foreach(Address a in aList)
            {
                _addresses.Add(a);
            }
        }


        public ModifyAddressPage()
        {
            this.InitializeComponent();

            setAddressess(APIConnector.GetAllAddressess());
        }

        public void setText(Address a)
        {
            _actualizing = true;
            Street1.Text = a.Street1;
            Street2.Text = (a.Street2 is null) ? "" : a.Street2;
            FlatNumber.Text = (a.FlatNumber is null) ? "" : a.FlatNumber.ToString();
            City.Text = a.City;
            PostalCode.Text = a.PostalCode;
            
        }

        public void showAllAddressess()
        {
            StackPanelDeliveryAddress.Visibility = Visibility.Collapsed;
            setAddressess(APIConnector.GetAllAddressess());
            allAddresses.Visibility = Visibility.Visible;
            _editingAddress = null;
        }

        
        private void fromParce_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (parcelNumber.Text.Equals(""))
            {
                //nie moze byc puste
            }
            else
            {
                _editingAddress = APIConnector.GetAddressFromPackage(int.Parse(parcelNumber.Text));
            }

            setText(_editingAddress);
            _actualizing = false;
            StackPanelDeliveryAddress.Visibility = Visibility.Visible;
            allAddresses.Visibility = Visibility.Collapsed;

        }

        private void SaveChanges_Tapped(object sender, TappedRoutedEventArgs e)
        {
            APIConnector.UdpateAddress(_editingAddress);
            showAllAddressess();
        }

        private void allAddresses_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            _editingAddress = allAddresses.SelectedItem as Address;
            if (_editingAddress != null)
            {
                setText(_editingAddress);
                _actualizing = false;
                StackPanelDeliveryAddress.Visibility = Visibility.Visible;
                allAddresses.Visibility = Visibility.Collapsed;
            }
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            var tebox = sender as TextBox;

            if (!_actualizing)
            {
                switch (tebox.Name)
                {
                    case "Street1":
                        _editingAddress.Street1 = tebox.Text;
                        break;
                    case "Street2":
                        _editingAddress.Street2 = tebox.Text;
                        break;
                    case "FlatNumber":
                        if (!tebox.Text.Equals(""))
                        {
                            _editingAddress.FlatNumber = int.Parse(tebox.Text);
                        }
                        break;
                    case "City":
                        _editingAddress.City = tebox.Text;
                        break;
                    case "PostalCode":
                        _editingAddress.PostalCode = tebox.Text;
                        break;
                }
            }
            
        }

        private void Cancel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            showAllAddressess();
        }
    }
}
