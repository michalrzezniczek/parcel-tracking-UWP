﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using uniMobileUWP.Models;
using Windows.UI.Popups;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace uniMobileUWP
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class NewPackagePage : Page
    {
        public NewPackagePage()
        {
            this.InitializeComponent();
        }


        private async void SaveButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Package p = new Package()
            {
                Weight = Double.Parse(Weight.Text),
                Width = Double.Parse(Width.Text),
                Height = Double.Parse(Height.Text),
                Depth = Double.Parse(Depth.Text),
                FkStatus = 1,
                PhoneNumber = PhoneNumber.Text
            };

            Address a = new Address()
            {
                Street1 = Street1.Text,
                Street2 = Street2.Text,
                City = City.Text,
                PostalCode = PostalCode.Text,

                FlatNumber = (FlatNumber.Text.Equals("")) ? -1 : int.Parse(FlatNumber.Text)
            };

            APIConnector.AddNewPackage(p, a);

            await new MessageDialog("Package added", "Congratulations").ShowAsync();
        }
    }
}
