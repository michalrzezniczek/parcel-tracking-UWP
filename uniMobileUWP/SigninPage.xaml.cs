﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace uniMobileUWP
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class SigninPage : Page
    {
        public SigninPage()
        {
            this.InitializeComponent();
        }

        async private void Button_Signin_Tapped(object sender, TappedRoutedEventArgs e)
        {
            bool signInSucceed = false;

            //wykonaj rejestrację
            Models.Employee employee = new Models.Employee()
            {
                FirstName = FirstName_TextBox.Text,
                LastName = LastName_TextBox.Text
            };

            Models.User user = new Models.User()
            {
                Login = login_TextBox.Text,
                Password = passwordBox.Password,
                PasswordHash = BitConverter.ToString(MD5.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(passwordBox.Password))).Replace("-", "").ToLower(),
                UserStatusId = 5
            };

            signInSucceed = Models.APIConnector.addUser(employee, user);


            if (signInSucceed)
            {
                await new MessageDialog("Sign-in successfully.", "Congratulation").ShowAsync();
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["logged"] = true;
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["username"] = user.Login;
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["userStatus"] = user.UserStatusId;
            }
            else
            {
                await new MessageDialog("Sign-in failed.", "Try again").ShowAsync();
            }
            
        }


        private void login_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (login_TextBox.Text.Equals("Login"))
            {
                login_TextBox.Text = "";
                login_TextBox.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void login_TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (login_TextBox.Text.Equals(""))
            {
                login_TextBox.Text = "Login";
                login_TextBox.Foreground = new SolidColorBrush(Colors.Gray);
            }
        }

        private void passwordBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (passwordBox.Password.Equals("Password"))
            {
                passwordBox.Password = "";
                passwordBox.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void passwordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (passwordBox.Password.Equals(""))
            {
                passwordBox.Password = "Password";
                passwordBox.Foreground = new SolidColorBrush(Colors.Gray);
            }
        }

        private void Email_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Email_TextBox.Text.Equals("yourMail@example.com"))
            {
                Email_TextBox.Text = "";
                Email_TextBox.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Email_TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Email_TextBox.Text.Equals(""))
            {
                Email_TextBox.Text = "yourMail@example.com";
                Email_TextBox.Foreground = new SolidColorBrush(Colors.Gray);
            }
        }

        private void FirstName_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (FirstName_TextBox.Text.Equals("First name"))
            {
                FirstName_TextBox.Text = "";
                FirstName_TextBox.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void FirstName_TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (FirstName_TextBox.Text.Equals(""))
            {
                FirstName_TextBox.Text = "First name";
                FirstName_TextBox.Foreground = new SolidColorBrush(Colors.Gray);
            }
        }

        private void LastName_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (LastName_TextBox.Text.Equals("Last name"))
            {
                LastName_TextBox.Text = "";
                LastName_TextBox.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void LastName_TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (LastName_TextBox.Text.Equals(""))
            {
                LastName_TextBox.Text = "Last name";
                LastName_TextBox.Foreground = new SolidColorBrush(Colors.Gray);
            }
        }
    }
}
