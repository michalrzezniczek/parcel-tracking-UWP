﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using uniMobileUWP.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace uniMobileUWP
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class StatusesPage : Page
    {
        private int _acutalSelectedStatus = 0;
        private Status _status;
        private bool _changes = false;
        private ObservableCollection<Status> _avaiableStatuses = new ObservableCollection<Status>();
        public ObservableCollection<Status> AviableStatuses { get { return _avaiableStatuses; } }
        private void setAviableStatuses(List<Status> l)
        {
            _avaiableStatuses.Clear();

            foreach (Status s in l)
            {
                _avaiableStatuses.Add(s);
            }
        }
        public StatusesPage()
        {
            this.InitializeComponent();
            setAviableStatuses(APIConnector.GetPackageStatuses());
        }

        private void packageStatuses_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            Edit_Button_Tapped(sender, new TappedRoutedEventArgs());
        }

        private void Save_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (_changes)
            {
                if(Name.Text.Equals(_status.Name) && Description.Text.Equals(_status.Description))
                {
                    _changes = false;
                }
                else
                {
                    _status.Name = Name.Text;
                    _status.Description = Description.Text;

                    APIConnector.UpdatePackageStatus(_status);
                }
            }
            else
            {
                if (Name.Text.Equals(""))
                {
                    //musi miec nazwe
                }
                else {
                    Status s = new Status()
                    {
                        Name = Name.Text,
                        Description = Description.Text
                    };

                    APIConnector.AddNewStatuPackage(s);
                }
            }

            /*
            editionStackPanel.Visibility = Visibility.Collapsed;
            setAviableStatuses(APIConnector.GetPackageStatuses());
            packageStatuses.Visibility = Visibility.Visible;
            */
            all_Button_Tapped(sender, e);
        }

        private void Add_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            editionStackPanel.Visibility = Visibility.Visible;
            packageStatuses.Visibility = Visibility.Collapsed;
            _changes = false;
            Name.Text = "";
            Description.Text = "";

        }

        private void Edit_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            editionStackPanel.Visibility = Visibility.Visible;
            packageStatuses.Visibility = Visibility.Collapsed;

            _status = APIConnector.GetPackageStatus(_acutalSelectedStatus);

            Name.Text = _status.Name;
            Description.Text = _status.Description;
            _changes = true;
        }

        private void Delete_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //APIConnector.deletePackageStatus(_acutalSelectedStatus);
        }

        private void packageStatuses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _acutalSelectedStatus = packageStatuses.SelectedIndex + 1;
            Edit_Button.Visibility = Visibility.Visible;
            Delete_Button.Visibility = Visibility.Visible;
        }

        private void all_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            editionStackPanel.Visibility = Visibility.Collapsed;
            setAviableStatuses(APIConnector.GetPackageStatuses());
            packageStatuses.Visibility = Visibility.Visible;
            Edit_Button.Visibility = Visibility.Collapsed;
            Delete_Button.Visibility = Visibility.Collapsed;
        }
    }
}
