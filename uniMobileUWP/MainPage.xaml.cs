﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x415

namespace uniMobileUWP
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private static ObservableCollection<NavLink> _navLinks = null;

        public ObservableCollection<NavLink> NavLinks
        {
            get { return _navLinks; }
        }

        public MainPage()
        {
            this.InitializeComponent();
            buildDefaultNavLinks();
        }

        private static void buildDefaultNavLinks()
        {
            if (_navLinks == null)
            {
                _navLinks = new ObservableCollection<NavLink>();
            }
            else
            {
                _navLinks.Clear();
            }


            _navLinks.Add(new NavLink() { Label = "Sign-in", Symbol = Windows.UI.Xaml.Controls.Symbol.AddFriend });
            _navLinks.Add(new NavLink() { Label = "Login", Symbol = Windows.UI.Xaml.Controls.Symbol.Permissions });
            _navLinks.Add(new NavLink() { Label = "Track", Symbol = Windows.UI.Xaml.Controls.Symbol.Find });
        }

        public static void buildNavLinks()
        {
            _navLinks.Clear();

            NavLink signIn = new NavLink() { Label = "Sign-in", Symbol = Windows.UI.Xaml.Controls.Symbol.AddFriend };
            NavLink login = new NavLink() { Label = "Login", Symbol = Windows.UI.Xaml.Controls.Symbol.Permissions };
            NavLink track = new NavLink() { Label = "Track", Symbol = Windows.UI.Xaml.Controls.Symbol.Find };
            NavLink logout = new NavLink() { Label = "Logout", Symbol = Windows.UI.Xaml.Controls.Symbol.Cancel };

            _navLinks.Add(track);

            
            if ((bool)Windows.Storage.ApplicationData.Current.LocalSettings.Values["logged"])
            {
                NavLink editStatusOfPackage = new NavLink() { Label = "Update status of parcel", Symbol = Windows.UI.Xaml.Controls.Symbol.Edit };
                NavLink addPackage = new NavLink() { Label = "Add parcel", Symbol = Windows.UI.Xaml.Controls.Symbol.Add };
                NavLink addAddress = new NavLink() { Label = "Modify addresses", Symbol = Windows.UI.Xaml.Controls.Symbol.Tag };
                NavLink addPackageStatus = new NavLink() { Label = "Parcel statuses", Symbol = Windows.UI.Xaml.Controls.Symbol.NewFolder };
                NavLink changeUserStatus = new NavLink() { Label = "Users", Symbol = Windows.UI.Xaml.Controls.Symbol.Admin };
                

                if ((int)Windows.Storage.ApplicationData.Current.LocalSettings.Values["userStatus"] <= 4)
                {
                    _navLinks.Add(editStatusOfPackage);
                }
                if ((int)Windows.Storage.ApplicationData.Current.LocalSettings.Values["userStatus"] <= 3)
                {
                    _navLinks.Add(addPackage);
                }
                if ((int)Windows.Storage.ApplicationData.Current.LocalSettings.Values["userStatus"] <= 2)
                {
                    _navLinks.Add(addAddress);
                }
                if ((int)Windows.Storage.ApplicationData.Current.LocalSettings.Values["userStatus"] <= 1)
                {
                    _navLinks.Add(addPackageStatus);
                    _navLinks.Add(changeUserStatus);
                }

                _navLinks.Add(logout);
            }
            else
            {
                _navLinks.Add(signIn);
                _navLinks.Add(login);
            }
        }

        private void HamburgerButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SplitView.IsPaneOpen = !SplitView.IsPaneOpen;
        }

        private void NavLinksList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var s = sender as ListView;
            var i = s.SelectedItem as NavLink;

            if (i is null)
            {

            }
            else
            {
                if (i.Label.Equals("Sign-in"))
                {
                    contentFrame.Navigate(typeof(SigninPage));
                }
                else if (i.Label.Equals("Login"))
                {
                    contentFrame.Navigate(typeof(LoginPage));
                }
                else if (i.Label.Equals("Track"))
                {
                    contentFrame.Navigate(typeof(TrackPage));
                }
                else if (i.Label.Equals("Logout"))
                {
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values.Remove("logged");
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values.Remove("username");
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values.Remove("userStatus");
                    this.Frame.Navigate(typeof(MainPage));
                }
                else if (i.Label.Equals("Update status of parcel"))
                {
                    contentFrame.Navigate(typeof(CourierPage));
                }
                else if (i.Label.Equals("Add parcel"))
                {
                    contentFrame.Navigate(typeof(NewPackagePage));
                }
                else if(i.Label.Equals("Modify addresses"))
                {
                    contentFrame.Navigate(typeof(ModifyAddressPage));
                }
                else if (i.Label.Equals("Parcel statuses"))
                {
                    contentFrame.Navigate(typeof(StatusesPage));
                }
                else if (i.Label.Equals("Users"))
                {
                    contentFrame.Navigate(typeof(UsersPage)); 
                }
            }
        }
    }

    public class NavLink
    {
        public string Label { get; set; }
        public Symbol Symbol { get; set; }
    }
}
