﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using uniMobileUWP.Models;
using System.Security.Cryptography;
using System.Threading.Tasks;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace uniMobileUWP
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        private List<User> _users;
         public LoginPage()
        {
            this.InitializeComponent();
            _users = Models.APIConnector.GetUsers();
            
        }
        
        async private void Button_Login_Tapped(object sender, TappedRoutedEventArgs e)
        {
            string login = login_TextBox.Text;
            string password = passwordBox.Password;

            if (login.Equals("Login") || password.Equals("Password"))
            {
                await new MessageDialog("Wrong login and/or password.", "Try again").ShowAsync();
                return;
            }
            bool loginSucceed = false;

            var loging_user = _users.FirstOrDefault(u => u.Login == login);

            if (loging_user != null)
            {
                
                var passMD5 = MD5.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                password = BitConverter.ToString(passMD5);
                password = password.Replace("-", "").ToLower();
                if (loging_user.PasswordHash.Equals(password))
                {
                    loginSucceed = true;
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["logged"] = true;
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["username"] = loging_user.Login;
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["userStatus"] = loging_user.UserStatusId;
                }
            }

            if (loginSucceed)
            {
                await new MessageDialog("Congratulations " + loging_user.Login, "Login accepted").ShowAsync();
                MainPage.buildNavLinks();
                this.Frame.Navigate(typeof(TrackPage));
            }
            else
            {
                await new MessageDialog("Wrong login and/or password.", "Try again").ShowAsync();
            }
        }

        private void login_TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (login_TextBox.Text.Equals("Login"))
            {
                login_TextBox.Text = "";
                login_TextBox.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void login_TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (login_TextBox.Text.Equals(""))
            {
                login_TextBox.Text = "Login";
                login_TextBox.Foreground = new SolidColorBrush(Colors.Gray);
            }
        }

        private void passwordBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (passwordBox.Password.Equals("Password"))
            {
                passwordBox.Password = "";
                passwordBox.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void passwordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (passwordBox.Password.Equals(""))
            {
                passwordBox.Password = "Password";
                passwordBox.Foreground = new SolidColorBrush(Colors.Gray);
            }
        }
    }
}
