﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace uniMobileUWP.Models
{
    class APIConnector
    {
        private static readonly HttpClient client = new HttpClient();

        private static string _apiAddress = "http://testparceltracking-rzezniczek.azurewebsites.net/api/";


        public static string apiAddress
        {
            get { return _apiAddress; }
        }
        
        //Logowanie -all
        public static List<User> GetUsers()
        {
            List<User> result;

            string url = apiAddress + "Users";
            //var response = client.GetStringAsync(url).Result;

            var response = Task.Run(() => client.GetStringAsync(url));

            

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<User>>(response.Result);
            foreach (User u in result)
            {
                u.Login = u.Login.Replace(" ", "");
                u.Password = u.Password.Replace(" ", "");
            }
            return result;
        }
        
        //Rejestracja -all
        public static bool addUser(Employee employee, User user)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(employee);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Employees";

            var httpResponse = client.PostAsync(url, httpContent).Result;
            if (httpResponse.Content != null)
            {
                var responseContent = httpResponse.Content.ReadAsStringAsync();
                employee = Newtonsoft.Json.JsonConvert.DeserializeObject<Employee>(responseContent.Result);
            }


            user.EmployeeId = employee.EmployeeId;
            json = Newtonsoft.Json.JsonConvert.SerializeObject(user);
            httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            url = apiAddress + "Users";

            httpResponse = client.PostAsync(url, httpContent).Result;

            return httpResponse.IsSuccessStatusCode;
        }

        //Tracking -all
        public static OldStatuses TrackPackage(int ID)
        {
            OldStatuses result;

            string url = apiAddress + "OldStatuses/" + ID.ToString();
            var response = Task.Run(() => client.GetStringAsync(url)).Result;

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<OldStatuses>(response);

            return result;
        }

        //Update status of package - courier
        public static void updateStatusOfPackage(int ID, int newStatus)
        {
            Package p = GetPackage(ID);
            p.FkStatus = newStatus;
            updatePackage(p);
        }
        public static Package GetPackage(int ID)
        {
            Package result;

            string url = apiAddress + "Packages/" + ID.ToString();
            var response = Task.Run(() => client.GetStringAsync(url)).Result;

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<Package>(response);

            return result;
        }
        public static Address GetAddressFromPackage(int ID)
        {
            Address result;

            Package p = GetPackage(ID);

            string url = apiAddress + "Addresses/" + p.FkDeliveryAddress.ToString();
            var response = Task.Run(() => client.GetStringAsync(url)).Result;

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<Address>(response);

            return result;
        }
        
        //Add new parcel - worker
        public static Package AddNewPackage(Package p, Address a)
        {
            Address addr = addAddress(a);
            Package result = null;

            p.FkDeliveryAddress = addr.AddressId;

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(p);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Packages";

            var httpResponse = client.PostAsync(url, httpContent).Result;

            if (httpResponse.Content != null)
            {
                var responseContent = httpResponse.Content.ReadAsStringAsync();
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<Package>(responseContent.Result);
            }

            return result;
        }

        //Add new address - admin
        public static Address addAddress(Address a)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(a);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Addresses";

            var httpResponse = client.PostAsync(url, httpContent).Result;
            if (httpResponse.Content != null)
            {
                var responseContent = httpResponse.Content.ReadAsStringAsync();
                a = Newtonsoft.Json.JsonConvert.DeserializeObject<Address>(responseContent.Result);
            }
            return a;
        }
        public static List<Address> GetAllAddressess()
        {
            List<Address> result;

            string url = apiAddress + "Addresses";
            var response = Task.Run(() => client.GetStringAsync(url));


            result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Address>>(response.Result);

            return result;
        }
        public static Address GetAddress(int ID)
        {
            Address result;

            string url = apiAddress + "Addresses/" + ID.ToString();
            var response = Task.Run(() => client.GetStringAsync(url));


            result = Newtonsoft.Json.JsonConvert.DeserializeObject<Address>(response.Result);

            return result;
        }
        public async static void UdpateAddress(Address a)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(a);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Addresses/" + a.AddressId.ToString();

            var httpResponse = await client.PutAsync(url, httpContent);
        }

        //Add new status of parcel - root
        public async static void AddNewStatuPackage(Status s)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(s);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Status";

            var httpResponse = await client.PostAsync(url, httpContent);
        }
        public static List<Status> GetPackageStatuses()
        {
            List<Status> result;

            string url = apiAddress + "Status";
            var response = Task.Run(() => client.GetStringAsync(url));

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Status>>(response.Result);

            return result;
        }
        public static Status GetPackageStatus(int ID)
        {
            Status result;

            string url = apiAddress + "Status/" + ID.ToString();
            var response = Task.Run(() => client.GetStringAsync(url)).Result;

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<Status>(response);

            return result;
        }
        public async static void UpdatePackageStatus(Status s)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(s);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Status/" + s.StatusId.ToString();

            var httpResponse = await client.PutAsync(url, httpContent);
        }

        //Edit user status - root
        public static List<Employee> GetEmployees()
        {
            List<Employee> result;

            string url = apiAddress + "Employees";
            var response = Task.Run(() => client.GetStringAsync(url)).Result;

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employee>>(response);

            return result;
        }
        public static Employee GetEmployee(int ID)
        {
            Employee result;

            string url = apiAddress + "Employees/" + ID.ToString();
            var response = Task.Run(() => client.GetStringAsync(url)).Result;

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<Employee>(response);

            return result;
        }
        public static User GetUser(int ID)
        {
            User result;

            string url = apiAddress + "Users/" + ID.ToString();
            var response = Task.Run(() => client.GetStringAsync(url)).Result;

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(response);

            return result;
        }
        public async static void UpdateUserStatus(int ID, int newUserStatus)
        {
            User u = GetUser(ID);
            u.UserStatusId = newUserStatus;

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(u);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Users/" + ID.ToString();

            var httpResponse = await client.PutAsync(url, httpContent);
        }
        /*public async static void UpdateUser(User u, Employee e)
        {
            //oba obiekty trzeba zamieniac
            //to niżej jest błędne

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(u);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Users/" + ID.ToString();

            var httpResponse = await client.PutAsync(url, httpContent);
        }
        */


        //Another functions
        private async static void updatePackage(Package p)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(p);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var url = apiAddress + "Packages/" + p.PackageId.ToString();

            var httpResponse = await client.PutAsync(url, httpContent);
        }
        




        
        public static List<UserStatus> GetUserStatuses()
        {
            List<UserStatus> result;

            string url = apiAddress + "UserStatus";
            var response = Task.Run(() => client.GetStringAsync(url));


            result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserStatus>>(response.Result);

            return result;
        }
        
    }
}
