﻿using System;
using System.Collections.Generic;

namespace uniMobileUWP.Models
{
    public partial class Employee
    {
        public Employee()
        {
            Package = new HashSet<Package>();
        }

        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public User User { get; set; }
        public ICollection<Package> Package { get; set; }
    }
}
