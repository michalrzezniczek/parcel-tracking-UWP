﻿using System;
using System.Collections.Generic;

namespace uniMobileUWP.Models
{
    public partial class Package
    {
        public int PackageId { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Depth { get; set; }
        public double Weight { get; set; }
        public int FkDeliveryAddress { get; set; }
        public string PhoneNumber { get; set; }
        public int FkStatus { get; set; }
        public string OldStatuses { get; set; }
        public int? EmployeeId { get; set; }

        public Employee Employee { get; set; }
        public Address FkDeliveryAddressNavigation { get; set; }
        public Status FkStatusNavigation { get; set; }
        public OldStatuses OldStatusesNavigation { get; set; }
    }
}
