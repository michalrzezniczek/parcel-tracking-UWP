﻿using System;
using System.Collections.Generic;

namespace uniMobileUWP.Models
{
    public partial class User
    {
        public int EmployeeId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        //Convert.ToBase64String(MD5.Create().ComputeHash(System.Text.Encoding.ASCII.GetBytes("password")))
        public int UserStatusId { get; set; }

        public Employee Employee { get; set; }
        public UserStatus UserStatus { get; set; }
    }
}
