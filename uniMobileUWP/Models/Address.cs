﻿using System;
using System.Collections.Generic;

namespace uniMobileUWP.Models
{
    public partial class Address
    {
        public Address()
        {
            Package = new HashSet<Package>();
            Status = new HashSet<Status>();
        }

        public int AddressId { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public int? FlatNumber { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public ICollection<Package> Package { get; set; }
        public ICollection<Status> Status { get; set; }
    }
}
