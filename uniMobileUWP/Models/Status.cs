﻿using System;
using System.Collections.Generic;

namespace uniMobileUWP.Models
{
    public partial class Status
    {
        public Status()
        {
            Package = new HashSet<Package>();
        }

        public int StatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? FkLocation { get; set; }

        public Address FkLocationNavigation { get; set; }
        public ICollection<Package> Package { get; set; }
    }
}
