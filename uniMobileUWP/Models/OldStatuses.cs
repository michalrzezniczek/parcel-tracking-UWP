﻿using System;
using System.Collections.Generic;

namespace uniMobileUWP.Models
{
    public partial class OldStatuses
    {
        public int PackageId { get; set; }
        public string ListOfIds { get; set; }
        public string ListOfDates { get; set; }

        public Package Package { get; set; }
    }
}
