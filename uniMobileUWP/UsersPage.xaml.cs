﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using uniMobileUWP.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace uniMobileUWP
{
    public sealed partial class UsersPage : Page
    {
        private ObservableCollection<User> _users = new ObservableCollection<User>();
        private ObservableCollection<UserStatus> _userStatuses = new ObservableCollection<UserStatus>();

        private User _actualSelectedUser;
        private int _actualSelectedUserStatus;
        private bool _changed = false;

        public ObservableCollection<User> Users { get { return _users; } }
        public ObservableCollection<UserStatus> UserStatuses { get { return _userStatuses; } }
        private void setUsers(List<User> listOfUsers)
        {
            _users.Clear();

            foreach(User u in listOfUsers)
            {
                u.UserStatus = _userStatuses.First(t => t.UserStatusId == u.UserStatusId);
                _users.Add(u);
            }
        }
        private void setUserStatuses(List<UserStatus> statuses)
        {
            _userStatuses.Clear();

            foreach(UserStatus us in statuses)
            {
                _userStatuses.Add(us);
            }
        }
        

        public UsersPage()
        {
            this.InitializeComponent();
            Edit_Button.Visibility = Visibility.Collapsed;

            setUserStatuses(APIConnector.GetUserStatuses());
            setUsers(APIConnector.GetUsers());
        }

        private void usersList_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            Edit_Button_Tapped(sender, new TappedRoutedEventArgs());
        }

        private void usersList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (usersList.SelectedIndex != -1)
            {
                _actualSelectedUser = _users[usersList.SelectedIndex];
                Edit_Button.Visibility = Visibility.Visible;
            }
        }

        private void Save_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (_changed)
            {
                APIConnector.UpdateUserStatus(_actualSelectedUser.EmployeeId, _actualSelectedUserStatus);
            }

            all_Button_Tapped(sender, e);
        }

        private void Edit_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            editionStackPanel.Visibility = Visibility.Visible;
            usersList.Visibility = Visibility.Collapsed;
            Edit_Button.Visibility = Visibility.Collapsed;

            Login.Text = _actualSelectedUser.Login;

            userStatusesList.SelectedIndex = _actualSelectedUser.UserStatusId - 1;
        }

        private void all_Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            editionStackPanel.Visibility = Visibility.Collapsed;

            setUserStatuses(APIConnector.GetUserStatuses());
            setUsers(APIConnector.GetUsers());

            usersList.Visibility = Visibility.Visible;
            Edit_Button.Visibility = Visibility.Collapsed;
        }

        private void userStatusesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _changed = true;
            _actualSelectedUserStatus = userStatusesList.SelectedIndex + 1;
        }
    }
}
