﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using uniMobileUWP.Models;
using System.Collections.ObjectModel;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace uniMobileUWP
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class CourierPage : Page
    {
        private bool _statusChanged = false;
        private int _newStatus;
        private ObservableCollection<Status> _avaiableStatuses = new ObservableCollection<Status>();
        public ObservableCollection<Status> AviableStatuses { get { return _avaiableStatuses; } }
        private void setAviableSTatuses(List<Status> l)
        {
            _avaiableStatuses.Clear();

            foreach (Status s in l)
            {
                _avaiableStatuses.Add(s);
            }
        }

        private Package _package;
        public Package Package { get { return _package; } }
        public CourierPage()
        {
            this.InitializeComponent();
        }

        private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            packageID.Text = parcelNumber.Text;

            setAviableSTatuses(APIConnector.GetPackageStatuses());

            _package = APIConnector.GetPackage(int.Parse(packageID.Text));

            _package.FkDeliveryAddressNavigation = APIConnector.GetAddressFromPackage(int.Parse(packageID.Text));

            phoneNumber.Text = _package.PhoneNumber;
            packageStreet1.Text = _package.FkDeliveryAddressNavigation.Street1;
            packageStreet2.Text = _package.FkDeliveryAddressNavigation.Street2;
            packageFlatNumber.Text = _package.FkDeliveryAddressNavigation.FlatNumber.ToString();
            packagePostalCode.Text = _package.FkDeliveryAddressNavigation.PostalCode;
            packageCity.Text = _package.FkDeliveryAddressNavigation.City;
            

            packageStatus.SelectedIndex = _package.FkStatus -1;

            Content.Visibility = Visibility.Visible;
        }

        private void Save_button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (_statusChanged)
            {
                _statusChanged = false;
                APIConnector.updateStatusOfPackage(int.Parse(packageID.Text), _newStatus);
            }
        }

        private void parcelNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            Content.Visibility = Visibility.Collapsed;
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listView = sender as ListView;
            if(listView.SelectedIndex != (_package.FkStatus-1))
            {
                _newStatus = listView.SelectedIndex + 1;
                _statusChanged = true;
            }
            else
            {
                _statusChanged = false;
            }
        }

        private void Button_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            //jesli mobile to cos tam
        }
    }
}
